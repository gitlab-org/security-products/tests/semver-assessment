require 'open3'

module VersionMatcher
  class VrangeError < MatcherError; end

  class Vrange
    GEMNASIUM_DB_REF_NAME = 'v2.0.4003'

    def initialize(cache)
      @cache = cache
    end

    def self.init(cache_manager)
      cache_name = "vrange.#{GEMNASIUM_DB_REF_NAME}"
      c = cache_manager.cache_for(cache_name)
      Vrange.new(c)
    end

    def matches_for(package_manager, package, versions)
      pkgs_json = { package: package, versions: versions }.to_json
      cmd = get_cmd(pkgs_json, package_manager)
      output = @cache.fetch(cache_key(cmd), -> { run(cmd) })
      format(output)
    end

    private

    def cache_key(cmd)
      x = cmd.index('/analyzer')
      cmd[x..]
    end

    def get_cmd(pkgs_json, type)
      "docker run --rm -e GEMNASIUM_DB_UPDATE_DISABLED=true -e GEMNASIUM_DB_REF_NAME=#{GEMNASIUM_DB_REF_NAME} vrangecheck /analyzer vrangecheck '#{pkgs_json}' #{type}"
    end

    def format(json_str)
      res = []
      vres = JSON.parse(json_str)
      vres.each do |r|
        dep = r['dependency']
        adv = r['advisory']
        res << Record.new(dep['name'], dep['version'], adv['identifier'], adv['affected_range'])
      end
      res
    end

    def run(cmd)
      stdout, stderr, status = Open3.capture3(cmd)
      if status.exitstatus != 0 
        raise StandardError, "vrange: error when fetching #{cmd} - #{stderr}"
      end
      stdout.split("\n").last
    end

    def inspect
      "@#{cache}"
    end
  end
end
