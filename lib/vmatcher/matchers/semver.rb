require 'semver_dialects'

module VersionMatcher
  class SemverError < MatcherError; end

  class Semver
    def initialize(advdb)
      @advisorydb = advdb
    end

    def matches_for(pkgmgr, pkg, vers)
      matches = []
      @advisorydb.advs(pkgmgr, pkg).each do |adv|
        rng = adv['affected_range']
        id = adv['identifier']
        vers.each do |ver|
          sat = SemverDialects::VersionChecker.version_sat?(pkgmgr, ver, rng)
          next unless sat
          matches << Record.new(pkg, ver, id, rng)
        end
      end
      matches
    end
  end
end
