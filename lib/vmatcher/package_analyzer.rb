module VersionMatcher
  class PackageAnalyzer
    def initialize(command_manager, cache_manager, semver, vrange)
      @command_manager = command_manager
      @cache_manager = cache_manager
      @semver = semver
      @vrange = vrange
    end

    def analyze(manager, package, report)
      Config.logger.debug("analyzing #{manager}/#{package}")
      command = @command_manager.command_for(manager, @cache_manager)

      begin 
        versions = command.fetch(package)
      rescue => e
        report.add_package_manager_error(package, e)
        return
      end

      report.add_package_versions(package, versions)

      begin
        report.add_vrange(@vrange.matches_for(manager, package, versions))
      rescue => e
        report.add_vrange_error(package, versions, e)
      end

      begin
        report.add_semver(@semver.matches_for(manager, package, versions))
      rescue => e
        report.add_semver_error(package, versions, e)
      end
    end
  end
end
