module VersionMatcher
  class NoopRequestRateController
    def initialize(backoff_s)
      #noop
    end

    def pause()
      # noop
    end
  end

  class RequestRateController
    def initialize(backoff_s)
      @last = nil
      @backoff_s = backoff_s
    end

    def pause()
      puts "pause"
      now = Time.now
      if @last.nil?
        last = now
        return
      end

      elapsed = (now - @last).to_f.truncate(1)
      if elapsed < @backoff_s
        sleep(@backoff_s - elapsed)
      end
      @last = now
    end
  end
end
class NoopRateController
  def initialize(*args)
  end

  def pause()
    puts "noop pause"
    # noop
  end
end
