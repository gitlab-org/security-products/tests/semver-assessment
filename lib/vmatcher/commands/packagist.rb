require 'uri'
require 'net/http'

module VersionMatcher
  class Packagist < Command
    def fetch(pkg)
      cmd = get_cmd(pkg)
      output = @cache.fetch(cmd, -> { fetch_from_packagist(pkg) })
      format(output)
    end

    private

    def get_cmd(pkg)
      "get curl https://packagist.org/packages/#{pkg}.json"
    end

    def create_uri(pkg)
      URI("https://packagist.org/packages/#{pkg}.json")
    end

    def fetch_from_packagist(pkg)
      @rate_controller.pause()

      uri = create_uri(pkg)
      resp = Net::HTTP.get_response(uri)
      unless resp.is_a?(Net::HTTPSuccess)
        raise StandardError, "packagist: error when fetching #{uri} - #{resp.code}"
      end
      resp.body
    end

    def format(str)
      obj = JSON.parse(str)
      versions = obj.dig('package', 'versions')
      if versions.nil?
        puts "error getting versions from #{obj}"
        return []
      end
      versions.keys
    end
  end
end
