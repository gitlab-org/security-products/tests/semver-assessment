module VersionMatcher
  class VersionCommand
    def initialize(cache, rate_controller)
      @cache = cache
      @rate_controller = rate_controller
    end

    def versions_for(pkg)
      cmd = get_cmd(pkg)
      output = @cache.fetch(cmd, -> { run(cmd) })
      format_versions(output)
    end

    private

    def run(cmd)
      @rate_controller.pause()
      stdout, stderr, status = Open3.capture3(cmd)
      if status.exitstatus != 0 
        raise VersionCommandFetchError.new(stdout, stderr)
      end
      stdout
    end

    def get_cmd(pkg)
      raise NotImplementedError
    end

    def format_versions(str)
      raise NotImplementedError
    end
  end
end
