module VersionMatcher
  class Command
    def initialize(cache, rate_controller)
      @cache = cache
      @rate_controller = rate_controller
    end

    # todo: rename fetch
    def fetch(pkg)
      @pkg = pkg
      cmd = get_cmd(pkg)
      output = @cache.fetch(cmd, -> { run(cmd) })
      format(output)
    end

    private

    def run(cmd)
      @rate_controller.pause()
      stdout, stderr, status = Open3.capture3(cmd)
      if status.exitstatus != 0 
        raise Error, "#{self.class} error #{stderr}"
      end
      stdout
    end

    def get_cmd(pkg)
      raise NotImplementedError
    end

    def format(str)
      raise NotImplementedError
    end
  end
end
