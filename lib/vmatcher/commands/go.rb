module VersionMatcher
  class Go < Command
    private

    def get_cmd(pkg)
      safe_pkg = pkg.downcase
      "docker run --rm golang:1.18 go list -m -versions #{safe_pkg}"
    end

    def format(str)
      matches = str.scan(/([^ |\n]+)/)
      if matches.length < 1
        return []
      end
      matches[1..].flatten
    end
  end
end
