require 'msgpack'

module VersionMatcher
  class CacheManager
    def initialize(cache_dir, ignore_cache_miss)
      @caches = {}
      @cache_dir = cache_dir
      @ignore_cache_miss = ignore_cache_miss
    end

    def cache_for(name)
      unless @caches.has_key?(name)
        @caches[name] = Cache.make(name, @cache_dir, @ignore_cache_miss)
      end
      @caches[name]
    end

    def flush_caches()
      @caches.each { |name, cache| cache.flush(true) }
    end

    def inspect
      "CacheManager: #{@caches.keys}"
    end
  end
end

module VersionMatcher
  class Cache

    # because flushing to disk is generally an expensive operation control
    # the number of calls to do before this happen
    FLUSH_EVERY_N_TIMES = 20

    attr_accessor :cache

    def initialize(path, ignore_cache_miss)
      @path = path
      @cache = {}
      @flush_times = 0
      @ignore_cache_miss = ignore_cache_miss
    end

    def self.make(name, cachedir, ignore_cache_miss)
      path = File.join(cachedir, "#{name}.cache.msgpack")
      Cache.new(path, ignore_cache_miss).tap do |c|
        c.hydrate()
      end
    end

    def fetch(key, callable)
      return @cache[key] if @cache.has_key?(key)

      Config.logger.debug("(#{@cache_dir}) cache miss (key fragment: '#{key[100..400]}')")
      if @ignore_cache_miss
        Config.logger.debug("ignoring")
        return nil
      end

      @cache[key] = callable.call()
      flush()
      @cache[key]
    end

    def hydrate
      if File.exist?(@path)
        @cache = MessagePack.unpack(File.binread(@path))
      end
    end

    def flush(force_flush=false)
      @flush_times += 1
      return unless force_flush || @flush_times >= FLUSH_EVERY_N_TIMES

      @flush_times = 0
      begin
      @cache.transform_values! { |v| v.force_encoding('UTF-8') }
      rescue => e
        byebug  
      end

      File.binwrite(@path, MessagePack.pack(@cache))
    end
  end

end
