module VersionMatcher
  class ReportPresenter
    def initialize(report, output=STDOUT)
      @report = report
      @output = output
      @lines = []
    end

    def group_by_package_range(records)
      result = {}
      records.each do |record|
        result[record.pkg] ||= {}
        result[record.pkg][record.rng] ||= []
        result[record.pkg][record.rng] << record
      end
      result
    end

    private

    def writel(str, indent=0, pad=0)
      @lines ||= []
      @lines << line(str + "\n", indent, pad)
    end

    def line(str, indent, pad)
      ('  ' * indent) + "#{str.center(pad)}"
    end
    
    def write(str, indent=0, pad=0)
      @output.write(line(str, indent, pad))
    end

    def flush
      return unless @lines.count > 0
      @output.write(@lines.join(''))
      @lines = []
    end
  end

  class MatchReportPresenter < ReportPresenter
    def print_results
      print_grouped_by_range('matches only in vrange', @report.only_in_vrange)
      print_grouped_by_range('matches only in semver', @report.only_in_semver)
    end

    def print_grouped_by_range(title, records) 
      write(title)
      group_by_package_range(records).each do |pkg, rng_records|
        write "pkg=#{pkg}:"
        rng_records.each do |rng, records|
          versions = records.map { |rec| rec.ver }.join(', ')
          write(" - range #{rng}")
          write(" - versions matched: #{versions}")
        end
      end
      write
      write '-'*120
    end
  end

  class MismatchReportPresenter < ReportPresenter
    def print_results
      write("#{@report.manager} missing records report")

      obj = matched_versions_diff
      diff = obj['missing']
      evaluated = obj['evaluated']
      diff['vrange'].each do |package, range_hash|
        write("package: #{package}", 1)

        all_versions = @report.package_versions[package] || []
        write("- evaluated the following versions: #{all_versions}", 2)

        write("- against these ranges: #{evaluated[package]}", 2)
        range_hash.each do |range, v_versions|
          write("- range #{range}", 3)
          s_versions = diff['semver'][package][range]
          if s_versions.count == all_versions.count
            write("- vrange missing: all", 4)
          else
            write("- vrange missing: #{s_versions}", 4)
          end
          if v_versions.count == all_versions.count
            write("- semver missing: all", 4)
          else
            write("- semver missing: #{v_versions}", 4)
          end
        end
        write('-'*120)
        write('')
      end
    end

    def matched_versions_diff
      vrange_only = group_by_package_range(@report.only_in_vrange)
      semver_only = group_by_package_range(@report.only_in_semver)

      result = {'vrange' => {}, 'semver' => {}}
      evaluated = {}

      vrange_only.each do |package, range_records|
        result['vrange'][package] ||= {}
        result['semver'][package] ||= {}

        range_records.each do |range, records|
          evaluated[package] ||= []
          evaluated[package] << range

          result['vrange'][package][range] ||= {}
          result['semver'][package][range] ||= {}

          vrange_found = records.map { |r| r.ver }
          s = semver_only.dig(package, range)
          semver_found = s.nil? ? [] : s.map { |r| r.ver }

          not_in_semver = semver_found-vrange_found
          not_in_vrange = vrange_found-semver_found

          result['vrange'][package][range] = not_in_semver
          result['semver'][package][range] = not_in_vrange
        end
      end

      semver_only.each do |package, range_records|
        result['vrange'][package] ||= {}
        result['semver'][package] ||= {}

        range_records.each do |range, records|
          evaluated[package] ||= []
          evaluated[package] << range

          result['vrange'][package][range] ||= {}
          result['semver'][package][range] ||= {}

          semver_found = records.map { |r| r.ver }
          v = vrange_only.dig(package, range)
          vrange_found = v.nil? ? [] : v.map { |r| r.ver }

          not_in_vrange = semver_found-vrange_found
          not_in_semver = vrange_found-semver_found

          result['vrange'][package][range] = not_in_vrange
          result['semver'][package][range] = not_in_semver
        end
      end
      { 'missing' => result,
        'evaluated' => evaluated }
    end
  end

  class DetailedPresenter < ReportPresenter
    def print_results
      vgroup = group_by_package_range(@report.vrange)
      sgroup = group_by_package_range(@report.semver)

      packages = (vgroup.keys + sgroup.keys).uniq

      packages.each do |package|
        if vgroup[package].nil? || sgroup[package].nil?
          writel("#{package}", 0)
          writel("-> not matched", 0)
          next
        end
        ranges = (vgroup[package].keys + sgroup[package].keys).uniq
        ranges.each do |range|
          varr = vgroup.dig(package, range)
          sarr = sgroup.dig(package, range)

          # todo: investigate this error - this means vrange threw an error
          # on this range
          next if varr.nil?

          # todo: investigate this error - this means semver threw an error
          # on this range
          next if sarr.nil?

          varr = varr.map { |rec| rec.ver }.sort
          sarr = sarr.map { |rec| rec.ver }.sort

          varr2 = (varr-sarr)
          sarr2 = (sarr-varr)

          has_results = (varr2.count - sarr2.count) != 0
          skip_when_no_results = true

          next if !has_results && skip_when_no_results

          writel("#{package} (affected: '#{range}')", 0)
          writel('-'*60)
          unless has_results 
            writel(" - no difference between vrange and semver", 0)
          else
            print_arrs(varr2, sarr2, col_width=30)
          end
          writel('')
        end
      end
      flush()
    end

    def print_arrs(arr1, arr2, col_width=50)
      writel("#{'only in vrange'.center(col_width)} | #{'only in semver'.center(col_width)}", 0)
      l1 = arr1.count
      l2 = arr2.count
      lm = l1 > l2 ? l1 : l2

      (0..lm-1).each do |i|
        a1 = i < l1 ? arr1[i] : '-'
        a2 = i < l2 ? arr2[i] : '-'
        writel("#{a1.center(col_width)} | #{a2.center(col_width)}", 0)
      end
    end
  end

  class StatsPresenter < ReportPresenter
    def print_results
      writel("#{@report.manager} analysis stats")
      writel(" - evaluated #{@report.package_versions.count} packages")
      writel(" - got #{@report.package_manager_errors.count} package manager errors")
      writel(" - got #{@report.vrange_errors.count} vrange errors")
      writel(" - got #{@report.semver_errors.count} semver errors")
      writel('')
      flush()
    end
  end

  class ErrorPresenter < ReportPresenter
    def print_results
      writel("#{@report.manager} error stats")
      writel("package manager errors")
      @report.package_manager_errors.each do |pkg, other|
        writel(" - #{pkg} - #{other}")
      end
      writel("semver errors")
      @report.semver_errors.each do |pkg, other|
        writel(" - #{pkg} - #{other}")
      end
      writel("vrange errors")
      @report.vrange_errors.each do |pkg, other|
        writel(" - #{pkg} - #{other}")
      end
      writel('')
      flush()
    end
  end
  
end
