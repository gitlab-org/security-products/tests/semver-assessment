require 'rspec'
require './lib/vmatcher'

class MockCache < Cache
  def flush
    #noop
  end
end

RSpec.skip Cache do
  describe '#hydrate' do
    let(:cache_contents) { { 'foo' => 'bar' } }
    let(:cache_path) { 'spec/fixtures/cache/foo' }
    let(:cache) { MockCache.new(cache_path) }

    context 'cache file exists' do
      before { File.write(cache_path, JSON.dump(cache_contents)) }
      after { File.delete(cache_path)}

      it 'sets cache contents to file contents' do
        expect { cache.hydrate() }
          .to change { cache.cache }.from({}).to(cache_contents)
      end
    end

    context 'cache file does not exist' do
      it 'does not change the cache contents' do
        expect { cache.hydrate()}.not_to change{ cache.cache }
      end
    end
  end

  describe '#fetch' do
    subject(:cache) { MockCache.new('does-not-need-to-exist') }

    context 'key hit' do
      let(:key) { 'anykey' }
      let(:result) { CommandResult.new(true, 'foo', nil) }

      before { cache.cache[key] = 'foo' }

      it 'does not call call callable' do
        called = false
        expect { cache.fetch(key, -> { called = true; result })}
          .not_to change { called }
      end
    end

    context 'key miss' do
      context 'call success' do
        it 'returns the result value' do
          r1 = CommandResult.new(true, 'foo', nil)
          expect(cache.fetch('anykey', -> { r1 })).to eq('foo')
        end

        it 'sets the result of the call to the cache key' do
          r1 = CommandResult.new(true, 'foo', nil)
          expect{ cache.fetch('anykey', -> { r1 }) }
            .to change { cache.cache['anykey'] }.from(nil).to('foo')
        end
      end

      context 'call failure' do
        it 'returns nil' do
          r1 = CommandResult.new(false, 'foo', Exception.new('foo'))
          expect(cache.fetch('anykey', -> { r1 })).to eq(nil)
        end

        it 'does not set the cache' do
          r1 = CommandResult.new(false, 'foo', Exception.new('foo'))
          expect{ cache.fetch('anykey', -> { r1 }) }
            .not_to change { cache.cache['anykey'] }
        end
      end
    end
  end
end

