# Test semver gem

Compare semver_dialects gem with gemnasium/vrange.

## Requirements

docker and ruby.

### vrangecheck image

This image is used to test `vrange` matches and is built from a branch in the gemnasium project:
https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/tree/vrangecheck-command

```bash
git clone https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium.git
cd gemnasium
docker build --build-arg GO_VERSION -t vrangecheck -f build/gemnasium/alpine/Dockerfile .
```

## Usage


### Test package managers

`bundle exec ruby bin/vmatcher.rb package nuget filesystem-list-parameter-plugin --verbosity debug`

Output is writen to a file with format:
`vmatch-pkg-[PACKAGE_NAME]-for-[PACKAGE-MANAGER_NAME]`

### Test a package

`bundle exec ruby bin/vmatcher.rb package nuget filesystem-list-parameter-plugin --verbosity debug`

Output is writen to a file with format: `vmatch-pkg-mgr-[PACKAGE-MANAGER-NAME]`

### Other options

See `bin/vmatcher.rb`.  

## TODOs

- [x] result caching
- [x] simple DOS backoff
- [x] create dedicated repo
- [x] maven
- [x] pypi
- [x] gem
- [x] packagist
- [x] nuget
- [x] npm
- [x] go
- [x] report stats
- [ ] support conan (need to find a single source)
- [ ] build vrangecheck via single command
- [ ] investigate errors 
  - [ ] package request misses
  - [ ] vrange errors
  - [ ] semver errors
    - the version of the local gemnasium-db and vrange gemnasium-db does not seem to match in spite of forced variables
- [ ] add matching on `fixed_range` not just `affected_range`
- [ ] customize output (specifiy filename, stdout, etc)
